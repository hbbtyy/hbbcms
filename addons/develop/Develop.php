<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-06 14:41:53
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 16:17:46
 */
namespace addons\develop;
use think\Addons;

/**
 * 插件测试
 * @author byron sampson
 */
class Develop extends Addons{
    public $info = [
        'name' => 'develop',
        'title' => '调试模式设置',
        'description' => '设置调试模式',
        'status' => 1,
        'author' => 'CraspHB彬',
        'version' => '1.0',
        'img'    => './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png',
        'has_adminlist'=> 0 //是否有设置
    ];
    //数据库表前缀
    public $prefix = '';
    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        return true;
    }

    /**
     * 实现的adminIndex钩子方法
     * @return mixed
     */
    public function adminIndexRight($param){
        
        
        $log_size = 0;
        $log_file_cnt = 0;
        foreach (list_file(LOG_PATH) as $f) {
            if ($f ['isDir']) {
                foreach (list_file($f ['pathname'] . '/', '*.log') as $ff) {
                    if ($ff ['isFile']) {
                        $log_size += $ff ['size'];
                        $log_file_cnt++;
                    }
                }
            }
        }
        $this->assign('debug',config('app_debug'));
        $this->assign('trace',config('app_trace'));
        $this->assign('log_size',$log_size);
        $this->assign('log_file_cnt',$log_file_cnt);
        // 这里可以通过钩子来调用钩子模板
        return $this->fetch('debug');
    }

}
