<?php
namespace addons\develop\controller;

use think\addons\Controller;

class Index extends Controller{

    public function index(){
       $type = input('id');
       switch ($type){
       	  case 'debug':
       	      if(config('app_debug')){
       	      	  if(set_config('app_debug',false)){
       	      	  	 $this->success('关闭debug成功！');
       	      	  }else{
       	      	  	 $this->error('关闭debug失败！');
       	      	  }
       	      }else{
				  if(set_config('app_debug',true)){
       	      	  	 $this->success('开启debug成功！');
       	      	  }else{
       	      	  	 $this->error('开启debug失败！');
       	      	  }       	      	  
       	      }
       	  break;
		  case 'trace':
			 if(config('app_trace')){
       	      	  if(set_config('app_trace',false)){
       	      	  	 $this->success('关闭trace成功！');
       	      	  }else{
       	      	  	 $this->error('关闭trace失败！');
       	      	  }
       	      }else{
				  if(set_config('app_trace',true)){
       	      	  	 $this->success('开启trace成功！');
       	      	  }else{
       	      	  	 $this->error('开启trace失败！');
       	      	  }       	      	  
       	      }		      
       	  break;
       	  default:  
       	    if(remove_dir(LOG_PATH)){
       	    	$this->success('删除日志成功！');
   	      	}else{
   	      		$this->error('删除日志失败！');
   	      	}
       }
    }
}