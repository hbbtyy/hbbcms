<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-06 14:41:53
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 11:20:32
 */
namespace addons\hbbcms;
use think\Addons;

/**
 * 插件测试
 * @author byron sampson
 */
class Hbbcms extends Addons{
    public $info = [
        'name' => 'Hbbcms',
        'title' => 'HBBCMS信息展示',
        'description' => '展示HBBCMS的信息',
        'status' => 1,
        'author' => 'CraspHB彬',
        'version' => '1.0',
        'img'    => './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png',
        'has_adminlist'=> 0
    ];
    //数据库表前缀
    public $prefix = '';
    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        return true;
    }

    /**
     * 实现的adminIndex钩子方法
     * @return mixed
     */
    public function adminIndex($param){
        // 这里可以通过钩子来调用钩子模板
        return $this->fetch('cmsInfo');
    }

}
