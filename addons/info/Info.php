<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-06 14:41:53
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 16:18:21
 */
namespace addons\info;
use think\Addons;

/**
 * 插件测试
 * @author byron sampson
 */
class Info extends Addons{
    public $info = [
        'name' => 'Info',
        'title' => '系统信息展示',
        'description' => '展示系统的信息',
        'status' => 1,
        'author' => 'CraspHB彬',
        'version' => '1.0',
        'img'    => './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png',
        'has_adminlist'=> 0
    ];
    //数据库表前缀
    public $prefix = '';
    //默认设置界面
    public $custom_config = '';
    /**
     * 插件安装方法
     * @return bool
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall(){
        return true;
    }

    /**
     * 实现的adminIndex钩子方法
     * @return mixed
     */
    public function adminIndex($param){
        $info = [
            'hbbcms_version' => 'HBBCMS管理系统V1.0 ',
            'php_version' => PHP_VERSION,
            'php_os'  => PHP_OS,
            'think_version' => THINK_VERSION,
            'upload_max'  => ini_get('upload_max_filesize'),
            'run_type'   => $_SERVER["SERVER_SOFTWARE"],
            'login_ip'  => request()->ip()
        ];
        $this->assign('info',$info);
        // 这里可以通过钩子来调用钩子模板
        return $this->fetch('sysInfo');
    }

}
