<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-24 20:42:10
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-06-27 16:57:49
 */
namespace app\admin\behavior;
use think\Log;
use think\Request;
use think\Db;

class WebLog{
	public function run(&$params){
		$web_log = config('web_log');
		if($web_log['web_log_on']){
			$request = Request::instance();

			//记录的操作及请求类型
			$web_log_controller = $web_log['web_log_controller'];
			$web_log_request = $web_log['web_log_request'];

			$action = $request->module().'/'.$request->controller().'/'.$request->action();
			$method = $request->method();
			//过滤不记录的请求
			if(in_array($method,$web_log_request)){
				//过滤不记录的操作
                if(in_array($action,$web_log_controller)){
                	 $admin = Db::name('admin')->find(session('user.id'));
                	 $param = $request->param()?json_encode($request->param()):"";
                	  //加密密码等关键字段
                	 if($param){
                	 	$param = (array)json_decode($param);
                	 	if(in_array("password",array_keys($param))){
                	 		$param['password'] = "***";
                	 	}
                	 }
                   $auth_rule = Db::name('auth_rule')->where('name',$action)->find();
                	 $param = $param ? json_encode($param):"";
                	 $data = [
                          'admin_id' => $admin['id'],
                          'admin_username' => $admin['username'],
                          'action_name'   => $auth_rule['title'],
                          'action_ip'    => $request->ip(),
                          'addtime'      => time(),
                          'action_method' => $method,
                          'action_param' =>$param,
                          'action_url'   => $request->url(),
                	 ];
                	 Db::name('web_log')->data($data)->insert();
                }
			}
			
		}
        
	}
}