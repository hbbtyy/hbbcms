<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-01 13:38:27
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-09 10:57:06
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Request;
use app\admin\model\Addons as AddonsModel;
use think\Cache;
class Addons extends Base{
	/**
	 * 插件列表
	 * @return [type] [description]
	 */
	public function index(){
		$addonsModel = new AddonsModel();
		$addons = $addonsModel->get_addons_list();
		$this->assign('addons',$addons);
		return $this->fetch();       
	}
	/**
	 * 安装插件
	 * @return [type] [description]
	 */
    public function install(){
        $addon_name     =   strtolower(trim(input('name')));
        $addonsModel = new AddonsModel();
		if($addonsModel->install($addon_name)){
			Cache::set('hook_addons',null);
            $this->success('安装成功');
		}else{
			$this->error($addonsModel->getError());
		}
    }
	/**
	 * 卸载插件
	 * @return [type] [description]
	 */
    public function uninstall(){
        $addon_name     =   strtolower(trim(input('name')));
        $addonsModel = new AddonsModel();
		if($addonsModel->uninstall($addon_name)){
			Cache::set('hook_addons',null);
            $this->success('卸载成功');
		}else{
			$this->error($addonsModel->getError());
		}
    }   
    /**
     * 设置插件页面
     * @return [type] [description]
     */
    public function config(){
        $addon_name   =   strtolower(trim(input('name')));
        $addonsModel = new AddonsModel();
		if(!($addon_config = $addonsModel->config($addon_name))) $this->error($addonsModel->getError());
        $this->assign('addon',$addon_config['addon']);
        //dump($addon_config['addon']);die;
        if($addon_config['custom_config']){
        	$this->assign('custom_config',$this->fetch($addon_config['custom_config']));
        }else{
        	$this->assign('custom_config','');
        }
        return $this->fetch();
    }

    /**
     * 保存插件设置
     */
    public function set_config(){
        $id  =  input('id');
        $config = input('config/a');
        $res = Db::name('addons')->where("id",$id)->setField('config',json_encode($config));
        if($res !== false){
            $this->success('保存成功',url('admin/Addons/index'));
        }else{
            $this->error('保存失败',url('admin/Addons/index'));
        }
    } 
	/**
	 * 钩子列表
	 * @return [type] [description]
	 */
	public function hooks(){
		$where = [];
		$name = input('name','');
		if(Request::instance()->isPost()){
			$where['name'] = ['like','%'.$name.'%'];
		}		
		$hooks = Db::name('hooks')->where($where)->select();
		$this->assign('hooks',$hooks);
		$this->assign('name',$name);
		return $this->fetch();
	}
	/**
	 * 添加钩子
	 * @return [type] [description]
	 */
	public function addons_add(){
		return $this->fetch();
	}
	public function addons_runadd(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        $data['addtime'] = time();
	        if(Db::name('hooks')->insert($data)){
	        	$this->success('钩子添加成功',url('admin/Addons/hooks'));
	        }else{
	        	$this->error('钩子添加失败',url('admin/Addons/hooks'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Addons/hooks'));
	   }
	}
	/**
	 * 修改钩子
	 * @return [type] [description]
	 */
	public function addons_edit(){
		$id = input('id');
		$hooks = Db::name('hooks')->find($id);
		$this->assign('hooks',$hooks);
		return $this->fetch();
	}
	public function addons_runedit(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        if(Db::name('hooks')->update($data)){
	        	$this->success('钩子修改成功',url('admin/Addons/hooks'));
	        }else{
	        	$this->error('钩子修改失败',url('admin/Addons/hooks'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Addons/hooks'));
	   }
	}
	/**
	 * 删除管钩子
	 * @return [type] [description]
	 */
	public function addons_del(){
		if(Request::instance()->isAjax()){
	        $id = input('id');
	        if(Db::name('hooks')->delete($id)){
	        	$this->success('钩子删除成功',url('admin/Addons/hooks'));
	        }else{
	        	$this->error('钩子删除失败',url('admin/Addons/hooks'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Addons/hooks'));
	   }
	}
	public function status(){
		if(Request::instance()->isAjax()){
	        $id = input('id');
	        $addonsModel = new AddonsModel();
	        $res = $addonsModel->set_status($id);
	        if($res['success'] == 1){
	        	Cache::set('hook_addons',null);
	        	$this->success($res['msg'],url('admin/Addons/index'));
	        }else{
	        	Cache::set('hook_addons',null);
	        	$this->error($res['msg'],url('admin/Addons/index'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Addons/index'));
	   }		
	}
}