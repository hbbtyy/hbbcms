<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-20 20:18:42
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-06-28 14:51:42
 */
namespace app\admin\controller;
use think\Controller;
use app\admin\model\AuthRule;
use app\admin\model\Admin;
use think\Request;
use app\common\Common;

class Base extends Common{
    
	public function _initialize(){
		parent::_initialize();
		//判断当前状态是否登录
		if(!((new Admin())->is_login())) $this->error('请先登录',url('admin/Login/index'),true);
		$auth = new AuthRule();
		//检测权限
		if(!($auth->check_auth())) $this->error('没有权限');
		//当前菜单
		$menu_all = $auth->get_menu();
		//当前操作所有的父id包含自己，用户导航展开
		$id_arr = $auth->get_parents_id('true');
		//面包屑导航
		$bread_crumbs = $auth->get_bread_crumbs($id_arr);
		$this->assign('menu_all',$menu_all);
		$this->assign('id_arr',$id_arr);
		$this->assign('bread_crumbs',$bread_crumbs);
	}
}