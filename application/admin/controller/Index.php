<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-19 14:19:35
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 11:04:37
 */
namespace app\admin\controller;
use think\Controller;
use think\Auth;
use think\Db;
use think\Loader;

class Index extends Base{
	public function index(){
		//最新注册用户
		$user = Db::name('admin')->where('id','neq',1)->field('username,avatar,addtime')->order('id DESC')->limit(18)->select();
		$this->assign('user',$user);
		$user_xchar = [
		   [date("Y-m-d",strtotime("-6 day"))],
		   [date("Y-m-d",strtotime("-5 day"))],
		   [date("Y-m-d",strtotime("-4 day"))],
		   [date("Y-m-d",strtotime("-3 day"))],
		   [date("Y-m-d",strtotime("-2 day"))],
		   [date("Y-m-d",strtotime("-1 day"))],
           [date('Y-m-d',time())],            
		];	
		$this->assign('user_xchar',json_encode($user_xchar));	
		return $this->fetch();
	}
}