<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-19 15:22:44
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-06-28 14:52:22
 */
namespace app\admin\controller;
use think\Controller;
use think\captcha\Captcha;
use app\admin\model\Admin;
use think\Request;
use app\common\Common;

class Login extends Common{

	public function index(){
		if((new Admin())->is_login()){
         	$this->redirect('admin/Index/index');
         }
		return $this->fetch();
	}
	/**
	 * 登录
	 * @return [type] [description]
	 */
	public function do_login(){
       if(Request::instance()->isAjax()){
	       $username = input('username');
	       $password = input('password');
	       $verify = input('verify');
	       if(!($this->check_verify($verify))) $this->error('验证码错误');
	       $admin = new Admin();
	       if($admin->login($username,$password)){
	           $this->success('登录成功',url('admin/Index/index'));
	       }else{
	       	   $this->error($admin->getError());
	       }
	   }else{
	   	  $this->error('提交方式不正确',url('admin/Login/index'));
	   }
	}
	/**
	 * [verify description] 生成验证码
	 * @return [type] [验证码]
	 */
	public function verify(){
	    ob_clean();
        $captcha = new Captcha(config('captcha'));
        return $captcha->entry();
	}
	/**
	 * 验证码检测
	 * @param  [type] $code [description]
	 * @return [type]       [description]
	 */
	function check_verify($code){
	    $captcha = new Captcha();
	    return $captcha->check($code);
    }
    /**
     * 退出登录
     * @return [type] [description]
     */
    public function loginout(){
		session('admin_user',null);
		session('admin_user_sig',null);
		cookie('admin_user', null);
        cookie('admin_user_sig', null);
		$this->redirect('admin/Login/index');
	}
}