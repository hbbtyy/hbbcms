<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-02-24 14:44:37
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-02-27 09:17:40
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Request;
use app\admin\model\News as NewsModel;
class News extends Base{
	/**
	 * 新闻展示
	 * @return [type] [description]
	 */
	public function index(){
		$news_cate = Db::name('news_cate')->order('sort')->select();
		$data = $_POST;
		$newsModel = new NewsModel();
		$news = $newsModel->get_news($data);
		$this->assign('news_cate',$news_cate);
		$this->assign('page',$news->render());
		$this->assign('news',$news);
		if(Request::instance()->isPost()){
        	return $this->fetch('ajax_index');
        }else{
        	return $this->fetch();
        }       
	}
	/**
	 * 草稿箱
	 * @return [type] [description]
	 */
	public function news_trash(){
		$news_cate = Db::name('news_cate')->order('sort')->select();
		$data = $_POST;
		$newsModel = new NewsModel();
		$news = $newsModel->get_news($data,'1');
		$this->assign('news_cate',$news_cate);
		$this->assign('page',$news->render());
		$this->assign('news',$news);
		if(Request::instance()->isPost()){
        	return $this->fetch('ajax_news_trash');
        }else{
        	return $this->fetch();
        }              
	}

	/**
	 * 移入回收站
	 * @return [type] [description]
	 */
	public function news_trash_add(){
        if(Request::instance()->isAjax()){
	        $id = input('id');
	        if(Db::name('news')->where('id',$id)->setField('trash','1')){
	        	$this->success('成功移入回收站',url('admin/News/index'));
	        }else{
	        	$this->error('移入回收站失败',url('admin/News/index'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/News/index'));
	   }
	}
	/**
	 * 发布
	 */
	public function news_trash_remove(){
        if(Request::instance()->isAjax()){
	        $id = input('id');
	        if(Db::name('news')->where('id',$id)->setField('trash','0')){
	        	$this->success('发布成功',url('admin/News/news_trash'));
	        }else{
	        	$this->error('发布失败',url('admin/News/news_trash'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/News/news_trash'));
	   }
	}
    /**
	 * 修改回收站新闻
	 * @return [type] [description]
	 */
	public function news_trash_edit(){
		$news = Db::name('news')->find(input('id'));
		$news_cate = Db::name('news_cate')->order('sort')->select();
		$news_source = Db::name('news_source')->order('times DESC')->select();
		$this->assign('news_source',$news_source);		
		$this->assign('news_cate',$news_cate);
		$this->assign('news',$news);
		return $this->fetch();		
	}
	public function news_trash_runedit(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        $news = new NewsModel();
	        if($news->news_edit($data)){
	        	$this->success('新闻修改成功',url('admin/News/news_trash'));
	        }else{
	        	$this->error($news->getError(),url('admin/News/news_trash'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }       
	}	
	/**
	 * 新闻添加
	 * @return [type] [description]
	 */
	public function news_add(){
		$news_cate = Db::name('news_cate')->order('sort')->select();
		$news_source = Db::name('news_source')->order('times DESC')->select();
		$this->assign('news_source',$news_source);
		$this->assign('time',date('Y-m-d H:i'));
		$this->assign('news_cate',$news_cate);
		return $this->fetch();
	}
	public function news_runadd(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        $news = new NewsModel();
	        if($news->news_add($data)){
	        	$this->success('新闻添加成功',url('admin/News/index'));
	        }else{
	        	$this->error($admin->getError(),url('admin/News/index'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/News/index'));
	   }
	}
	/**
	 * 修改新闻
	 * @return [type] [description]
	 */
	public function news_edit(){
		$news = Db::name('news')->find(input('id'));
		$news_cate = Db::name('news_cate')->order('sort')->select();
		$news_source = Db::name('news_source')->order('times DESC')->select();
		$this->assign('news_source',$news_source);		
		$this->assign('news_cate',$news_cate);
		$this->assign('news',$news);
		return $this->fetch();		
	}
	public function news_runedit(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        $news = new NewsModel();
	        if($news->news_edit($data)){
	        	$this->success('新闻修改成功',url('admin/News/index'));
	        }else{
	        	$this->error($news->getError(),url('admin/News/index'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }       
	}
	/**
	 * 删除新闻
	 * @return [type] [description]
	 */
	public function news_del(){
		if(Request::instance()->isAjax()){
			$id = input('id/a');
			if(Db::name('news')->where('id','in',$id)->delete()){
	        	$this->success('新闻删除成功');
	        }else{
	        	$this->error('新闻删除失败');
	        }
		}else{
	   	  $this->error('提交方式不正确');
	    }
	}
	/**
	 * 更新排序
	 * @return [type] [description]
	 */
	public function news_sort(){
        if(Request::instance()->isAjax()){
        	$data = input('post.');
        	foreach($data as $k => $v){
        		Db::name('news')->where(['id'=>$k])->setField('sort',$v);	
        	}
        	$this->success('排序更新成功');
        }else{
	   	  $this->error('提交方式不正确');
	    }
	}	
	/**
	 * 更改新闻分类
	 * @return [type] [description]
	 */
    public function news_cid(){
       if (!request()->isAjax()) {
            $this->error('提交方式不正确', url('admin/News/index'));
        } else {
        	$cid = input('news_cid');
        	$id = input('n_id');
        	if(Db::name('news')->where('id',$id)->setField('cid',$cid)){
                $this->success('新闻分类修改成功');
        	}else{
               $this->error('新闻分类修改失败');
        	}
        }
    }
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function news_status(){
        $id = input('get.id');
        $newsModel = new NewsModel();
        $res = $newsModel->news_status($id);
        return $res;
	}    
	/**
	 * 文章分类列表
	 * @return [type] [description]
	 */
	public function news_cate(){
		$where = [];
		$name = input('name','');
		if(Request::instance()->isPost()){
			$where['name'] = ['like','%'.$name.'%'];
		}
        $news_cate = Db::name('news_cate')->where($where)->order('sort')->paginate(config('paginate.list_rows'));
        $page = $news_cate->render();
        $this->assign('news_cate',$news_cate);
        $this->assign('page',$page);
        $this->assign('name',$name);
        return $this->fetch();
	}
	/**
	 * 新闻分类添加
	 * @return [type] [description]
	 */
	public function news_cate_add(){
        return $this->fetch();
	}
	public function news_cate_runadd(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        if(Db::name('news_cate')->insert($data)){
	        	$this->success('新闻分类添加成功',url('admin/News/news_cate'));
	        }else{
	        	$this->error('新闻分类添加失败',url('admin/News/news_cate'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/News/news_cate'));
	   }
	}
	/**
	 * 修改新闻分类
	 * @return [type] [description]
	 */
	public function news_cate_edit(){
		$id = input('id');
		$news_cate = Db::name('news_cate')->find($id);
		$this->assign('news_cate',$news_cate);
		return $this->fetch();		
	}
	public function news_cate_runedit(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        if(!isset($data['status'])){
		        $data['status'] = '0';
		     }
	        $res = Db::name('news_cate')->update($data);
	        if($res){
	        	$this->success('新闻分类修改成功',url('admin/News/news_cate'));
	        }else{
	        	$this->error('新闻分类添加失败',url('admin/News/news_cate'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/News/news_cate'));
	   }
	}	
	/**
	 * 删除新闻分类
	 * @return [type] [description]
	 */
	public function news_cate_del(){
		if(Request::instance()->isAjax()){
	        $id = input('id');
	        if(Db::name('news_cate')->delete($id)){
	        	$this->success('新闻分类删除成功',url('admin/News/news_cate'));
	        }else{
	        	$this->error('新闻分类删除失败',url('admin/News/news_cate'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }
	}	
	/**
	 * 更新排序
	 * @return [type] [description]
	 */
	public function news_cate_sort(){
        if(Request::instance()->isAjax()){
        	$data = input('post.');
        	foreach($data as $k => $v){
        		Db::name('news_cate')->where(['id'=>$k])->setField('sort',$v);	
        	}
        	$this->success('排序更新成功',url('admin/News/news_cate'));
        }else{
	   	  $this->error('提交方式不正确',url('admin/News/news_cate'));
	    }
	}
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function news_cate_status(){
          $id = input('get.id');
          $status = Db::name('news_cate')->where('id',$id)->value('status');
	      $res = [];
	      if($status == '1'){
	        if(Db::name('news_cate')->where('id',$id)->setField('status','0')){
	          $res = ['code'=>'1','msg'=>'已禁用','btn'=>'0'];
	        }else{
	          $res = ['code'=>'0','msg'=>'修改失败'];
	        }
	      }else{
	          if(Db::name('news_cate')->where('id',$id)->setField('status','1')){
	          $res = ['code'=>'1','msg'=>'已开启','btn'=>'1'];
	        }else{
	          $res = ['code'=>'0','msg'=>'修改失败'];
	        }
	      }
	      return $res;
	}
}