<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-19 15:15:31
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-06-28 15:07:28
 */
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Admin;
use app\admin\model\AuthGroup;
use app\admin\model\AuthRule;
use app\admin\model\FrontMenu;
use think\Request;
use think\Db;

class Sys extends Base{
	/**
	 * 管理员列表
	 * @return [type] [description]
	 */
	public function admin(){
		$where = [];
		$username = input('username')?input('username'):'';
		if(Request::instance()->isPost()){
			$where['username'] = ['like','%'.$username.'%'];
		}
        $admin_list = Db::name('admin')->alias('a')->join(config('prefix').'auth_group_access b','b.uid = a.id')->join(config('prefix').'auth_group c','c.id = b.group_id')->where($where)->field('a.*,c.title')->select();
        $this->assign('admin_list',$admin_list);
        $this->assign('username',$username);
		return $this->fetch();
	}
	/**
	 * 添加管理员
	 * @return [type] [description]
	 */
	public function admin_add(){
		$role = Db::name('auth_group')->where('status','1')->select();
		$this->assign('role',$role);
		return $this->fetch();
	}
	public function admin_runadd(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        if(request()->file('avatar')){
        		$avatar = upload_img(request()->file('avatar'),'avatar');
	        	if(!$avatar['success']){
	        		$this->error($avatar['msg'],url('admin/Sys/admin'));
	        	}
	        	$data['avatar'] = $avatar['filename'];
        	}	    
	        $admin = new Admin();
	        if($admin->admin_add($data)){
	        	$this->success('管理员添加成功',url('admin/Sys/admin'));
	        }else{
	        	$this->error($admin->getError(),url('admin/Sys/admin'),$admin->getData());
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }
	}
	public function admin_edit(){
		$id = input('id');
		$admin = Db::name('admin')->find($id);
		$role = Db::name('auth_group')->where('status','1')->select();
		//找出关系
		$group_id = Db::name('auth_group_access')->where('uid',$id)->value('group_id');
		$this->assign('role',$role);
		$this->assign('admin',$admin);
		$this->assign('group_id',$group_id);
		return $this->fetch();
	}
	/**
	 * 修改管理员
	 * @return [type] [description]
	 */
	public function admin_runedit(){
		if(Request::instance()->isAjax()){
	        $data = $_POST;
	        if(request()->file('avatar')){
        		$avatar = upload_img(request()->file('avatar'),'avatar');
	        	if(!$avatar['success']){
	        		$this->error($avatar['msg'],url('admin/Sys/admin'));
	        	}
	        	$data['avatar'] = $avatar['filename'];
        	}else{
        		unset($data['avatar']);
        	}
	        $admin = new Admin();
	        if($admin->admin_edit($data)){
	        	$this->success('管理员修改成功',url('admin/Sys/admin'));
	        }else{
	        	$this->error($admin->getError(),url('admin/Sys/admin'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }
	}
	/**
	 * 删除管理员
	 * @return [type] [description]
	 */
	public function admin_del(){
		if(Request::instance()->isAjax()){
	        $id = input('id');
		 	//修改的情况
		     //  1：超级管理员并且id为1的可以修改所有的
		     //  2：其他超级管理员只能修改自己跟其他的
		     //  3：普通的只能修改自己的
		     $admin_id = session('admin_user.id');
		     if($admin_id == $id){
		          $this->error('没有删除权限',url('admin/Sys/admin'));              
		      }
		     if($admin_id != 1){
		        $group_id = Db::name('auth_group_access')->where('uid',$admin_id)->value('group_id');
		        //修改的group_id
		        $group_id_edit = Db::name('auth_group_access')->where('uid',$id)->value('group_id');
		        if($group_id == 1){
		             //超级管理员修改其他超级管理员 不允许
		            if($group_id_edit == 1){
		                $this->error('没有删除权限',url('admin/Sys/admin'));                
		            } 
		        }
		     }	        
	        if(Db::name('admin')->delete($id)){
	        	$this->success('管理员删除成功',url('admin/Sys/admin'));
	        }else{
	        	$this->error('管理员删除失败',url('admin/Sys/admin'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin'));
	   }
	}
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function admin_status(){
        $id = input('get.id');
        $admin = new Admin();
        $res = $admin->admin_status($id);
        return $res;
	}
	/**
	 * 角色列表
	 * @return [type] [description]
	 */
	public function admin_role(){
		$role = Db::name('auth_group')->select();
		$this->assign('role',$role);
		return $this->fetch();
	}
	public function admin_role_add(){
		$this->auth_menu();
		return $this->fetch();
	}
	/**
	 * 角色添加操作
	 * @return [type] [description]
	 */
	public function admin_role_runadd(){
       if(Request::instance()->isAjax()){
       	  $data = input('post.');
       	  $role = new AuthGroup();
       	  if($role->role_add($data)){
   	  		 $this->success('角色添加成功',url('admin/Sys/admin_role'));
          }else{
        	 $this->error($role->getError(),url('admin/Sys/admin_role'));
       	  }
       }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin_role'));
	   }
	}
	public function admin_role_edit(){
		 $id = input('id');
		 $role = Db::name('auth_group')->find($id);
		 $role['ids'] = explode(',',$role['rules']);
		 $this->auth_menu();
		 $this->assign('role',$role);
		 return $this->fetch();
	}
	/**
	 * 修改角色
	 * @return [type] [description]
	 */
	public function admin_role_runedit(){
		if(Request::instance()->isAjax()){
	        $data = input('post.');
	        $role = new AuthGroup();
	        if($role->role_edit($data)){
	        	$this->success('角色修改成功',url('admin/Sys/admin_role'));
	        }else{
	        	$this->error($role->getError(),url('admin/Sys/admin_role'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin_role'));
	   }
	}
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function admin_role_status(){
        $id = input('get.id');
        $role = new AuthGroup();
        $res = $role->role_status($id);
        return $res;
	}
	/**
	 * 删除角色
	 * @return [type] [description]
	 */
	public function admin_role_del(){
		if(Request::instance()->isAjax()){
	        $id = input('id');
	        if(Db::name('auth_group')->delete($id)){
	        	$this->success('角色删除成功',url('admin/Sys/admin_role'));
	        }else{
	        	$this->error('角色删除失败',url('admin/Sys/admin_role'));
	        }
	    }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/admin_role'));
	   }
	}
	/**
	 * 菜单列表
	 * @return [type] [description]
	 */
	public function menu(){
		$this->get_auth_arr();
		return $this->fetch();
	}
	public function menu_add(){
		//显示所有权限
		$this->get_auth_arr();
        $id = input('id')?input('id'):'0';
        $this->assign('id',$id);
        return $this->fetch();

	}
	/**
	 * [添加管理员]
	 * @return [type] [description]
	 */
	public function menu_runadd(){
		if(Request::instance()->isAjax()){
       	  $data = input('post.');
       	  $menu = new AuthRule();
       	  if($menu->menu_add($data)){
   	  		 $this->success('菜单添加成功',url('admin/Sys/menu'));
          }else{
        	 $this->error($menu->getError(),url('admin/Sys/menu'));
       	  }
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/menu'));
	    }
	}
	public function menu_edit(){
        $this->get_auth_arr();
        $edit_menu = Db::name('auth_rule')->find(input('id'));
        $this->assign('edit_menu',$edit_menu);
        return $this->fetch();
	}
	/**
	 * 修改菜单
	 * @return [type] [description]
	 */
	public function menu_runedit(){
		if(Request::instance()->isAjax()){
			$data = input('post.');
       	    $menu = new AuthRule();
       	    if($menu->menu_edit($data)){
	   	  		 $this->success('菜单修改成功',url('admin/Sys/menu'));
	          }else{
	        	 $this->error($role->getError(),url('admin/Sys/menu'));
	       	  }
		}else{
	   	  $this->error('提交方式不正确',url('admin/Sys/menu'));
	    }
	}
	public function menu_del(){
		if(Request::instance()->isAjax()){
			$id = input('id');
			//找到该菜单下所有子类id,包含自己
			$menu = new AuthRule();
			$child_id = $menu->get_child_id($id,true);
			if(Db::name('auth_rule')->where('id','in',$child_id)->delete()){
	        	$this->success('菜单删除成功',url('admin/Sys/menu'));
	        }else{
	        	$this->error('菜单删除失败',url('admin/Sys/menu'));
	        }
		}else{
	   	  $this->error('提交方式不正确',url('admin/Sys/menu'));
	    }
	}
	/**
	 * 更新排序
	 * @return [type] [description]
	 */
	public function menu_sort(){
        if(Request::instance()->isAjax()){
        	$data = input('post.');
        	foreach($data as $k => $v){
        		Db::name('auth_rule')->where(['id'=>$k])->setField('sort',$v);	
        	}
        	$this->success('排序更新成功',url('admin/Sys/menu'));
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/menu'));
	    }
	}
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function menu_status(){
        $id = input('get.id');
        $role = new AuthRule();
        $res = $role->menu_status($id);
        return $res;
	}
	/**
	 * 修改是否检测
	 * @return [type] [description]
	 */
	public function menu_notcheck(){
        $id = input('get.id');
        $role = new AuthRule();
        $res = $role->menu_notcheck($id);
        return $res;
	}
	/**
	 * 得到树状菜单列表
	 * @return [type] [description]
	 */
	protected function get_auth_arr(){
		$menu_all = Db::name('auth_rule')->order('sort')->select();
        $tree = new \Tree();
        $tree->init($menu_all);
        $menu = $tree->get_tree();
        $this->assign('menu',$menu);
	}
	/**
	 * 得到所有权限列表数组
	 * @return [type] [description]
	 */
	protected function auth_menu(){
		$menu = Db::name('auth_rule')->order('sort,id')->select();
		$tree = new \Tree();
		$tree->init($menu);
		$auth_menu = $tree->get_list();
		$this->assign('auth_menu',$auth_menu);
	}
	/**
	 * 日志列表
	 * @return [type] [description]
	 */
	public function web_log(){
		$where = [];
		$admin_id = input('admin_id');
		$action_method = input('action_method');
		if($admin_id != '') $where['admin_id'] = $admin_id;
		if($action_method != '') $where['action_method'] = $action_method;
		$admin = Db::name('admin')->field('id,username')->select();
		$web_log = Db::name('web_log')->where($where)->order('id desc')->paginate(config('paginate.list_rows'));
        $page = $web_log->render();
        $this->assign('page',$page);
        $this->assign('web_log',$web_log);
        $this->assign('admin',$admin);
        if(Request::instance()->isPost()){
        	return $this->fetch('ajax_web_log');
        }else{
        	return $this->fetch();
        }
		
	}
	public function web_log_runset(){
		if(Request::instance()->isAjax()){
        	$data = input('post.');
        	$data['web_log_on'] = input('status') ? input('status') :'0';
        	$data['web_log_request'] = input('web_log_request/a') ? input('web_log_request/a') :[];
        	$data['web_log_controller'] = input('web_log_controller/a') ? input('web_log_controller/a') :[];
        	$key = 'web_log';
        	if(set_config($key,$data)){
        		$this->success('日志设置成功',url('admin/Sys/sys_basic_weblog'));
        	}else{
        		$this->error('日志设置失败',url('admin/Sys/sys_basic_weblog'));
        	}
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/sys_basic_weblog'));
	    }
	}
	 /**
     * 删除日志
     * @return [type] [description]
     */
    public function web_log_del(){
       if(Request::instance()->isAjax()){
            $id = input('id/a');
            foreach($id as $k){
                if(!(Db::name('web_log')->delete($k))){
                    $this->error('日志删除失败',url('admin/Sys/web_log'));
                }
            }
            $this->success('日志删除成功',url('admin/Sys/web_log'));  
        }else{
          $this->error('提交方式不正确',url('admin/Sys/web_log'));
       }
    }

    ##########################系统参数设置#######################################3
    /**
     * 基本设置
     * @return [type] [description]
     */
    public function sys_basic_set(){
    	$web_basic = get_config('web_basic');
    	$this->assign('web_basic',$web_basic);
    	return $this->fetch();
    }
    public function sys_basic_runset(){
    	if(Request::instance()->isAjax()){
        	$data = input('post.');
        	$key = 'web_basic';
        	if(set_config($key,$data)){
        		$this->success('基本设置成功',url('admin/Sys/sys_basic_set'));
        	}else{
        		$this->error('基本设置失败',url('admin/Sys/sys_basic_set'));
        	}
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/sys_basic_set'));
	    }
    }
    /**
     * 系统设置
     * @return [type] [description]
     */
    public function sys_basic_sys(){
        $web_sys = get_config('web_sys');
    	$this->assign('web_sys',$web_sys);
        $captcha = get_config('captcha');
    	$this->assign('captcha',$captcha);    	   	
    	return $this->fetch();
    }
    public function sys_basic_runsys(){
		if(Request::instance()->isAjax()){
        	$data = input('post.');
        	if(request()->file('img')){
        		$site_logo = upload_img(request()->file('img'),'logo');
	        	if(!$site_logo['success']){
	        		$this->error($res['msg'],url('admin/Sys/sys_basic_sys'));
	        	}
	        	$data['site_logo'] = $site_logo['filename'];
        	}
        	$web_sys_data = [
				'site_name'=>$data['site_name'],
				'site_url' =>$data['site_url'],
				'site_logo'=>$data['site_logo'],
				'list_row' => $data['list_row'],               
        	];
        	$captcha = get_config('captcha');
        	$captcha_data = [
		           'fontttf'=>$data['fontttf'],  
		            'useNoise'    => $data['useNoise'], 
		            'useCurve'   =>$data['useCurve'],                 
        	];
        	$captcha_mer = array_merge($captcha,$captcha_data);
        	$res = set_config('web_sys',$web_sys_data);
        	$res2 = set_config('captcha',$captcha_mer);
        	if($res && $res2){
        		$this->success('系统设置成功',url('admin/Sys/sys_basic_sys'));
        	}else{
        		$this->error('系统设置失败',url('admin/Sys/sys_basic_sys'));
        	}
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/sys_basic_sys'));
	    }    	
    }
    /**
     * 数据库设置
     * @return [type] [description]
     */
    public function sys_basic_database(){
     	$datacon = get_config('datacon');
    	$this->assign('datacon',$datacon);
    	return $this->fetch();			
    }
    public function sys_basic_rundatabase(){
    	if(Request::instance()->isAjax()){
        	$data = input('post.');
        	$key = 'datacon';
        	if(set_config($key,$data)){
        		$this->success('数据库设置成功',url('admin/Sys/sys_basic_database'));
        	}else{
        		$this->error('数据库设置失败',url('admin/Sys/sys_basic_database'));
        	}
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/sys_basic_database'));
	    }
    }
    /**
     * 上传设置
     * @return [type] [description]
     */
    public function sys_upload_set(){
        $web_upload = get_config('web_upload');
    	$this->assign('web_upload',$web_upload);    	
    	return $this->fetch();
    }
    public function sys_upload_runset(){
       if(Request::instance()->isAjax()){
        	$data = input('post.');
        	if(request()->file('img')){
        		$water_img = upload_img(request()->file('img'),'logo');
	        	if(!$water_img['success']){
        		   $this->error($res['msg'],url('admin/Sys/sys_upload_set'));
        	    }
	        	$data['water_img'] = $water_img['filename'];
        	}
        	if(set_config('web_upload',$data)){
        		$this->success('上传设置成功',url('admin/Sys/sys_upload_set'));
        	}else{
        		$this->error('上传设置失败',url('admin/Sys/sys_upload_set'));
        	}
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/sys_upload_set'));
	    }
    }
    public function sys_basic_weblog(){
    	$this->get_auth_arr();
		$this->assign('web_log_set',get_config('web_log'));
		return $this->fetch();
    }
    /**
     * 前台菜单 
     * @return [type] [description]
     */
    public function front_menu(){
       $this->get_front_menu_arr();
       return $this->fetch();
    }
	/**
	 * 得到树状前台菜单列表
	 * @return [type] [description]
	 */
	protected function get_front_menu_arr(){
		$menu_all = Db::name('front_menu')->order('sort')->select();
        $tree = new \Tree();
        $tree->init($menu_all);
        $menu = $tree->get_tree();
        $this->assign('menu',$menu);
	}
	/**
	 * [添加前台菜单]
	 * @return [type] [description]
	 */
	public function front_menu_add(){
		$this->get_front_menu_arr();
		$id = input('id')?input('id'):'0';
        $this->assign('id',$id);		
        return $this->fetch();

	}
	public function front_menu_runadd(){
		if(Request::instance()->isAjax()){
       	  $data = input('post.');
       	  $menu = new FrontMenu();
       	  if($menu->menu_add($data)){
   	  		 $this->success('菜单添加成功',url('admin/Sys/front_menu'));
          }else{
        	 $this->error($menu->getError(),url('admin/Sys/front_menu'));
       	  }
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/front_menu'));
	    }
	}
	/**
	 * 修改菜单
	 * @return [type] [description]
	 */
	public function front_menu_edit(){
        $this->get_front_menu_arr();
        $edit_menu = Db::name('front_menu')->find(input('id'));
        $this->assign('edit_menu',$edit_menu);
        return $this->fetch();
	}
	
	public function front_menu_runedit(){
		if(Request::instance()->isAjax()){
			$data = input('post.');
       	    $menu = new FrontMenu();
       	    if($menu->menu_edit($data)){
	   	  		 $this->success('菜单修改成功',url('admin/Sys/front_menu'));
	          }else{
	        	 $this->error($menu->getError(),url('admin/Sys/front_menu'));
	       	  }
		}else{
	   	  $this->error('提交方式不正确',url('admin/Sys/front_menu'));
	    }
	}
	public function front_menu_del(){
		if(Request::instance()->isAjax()){
			$id = input('id');
			//找到该菜单下所有子类id,包含自己
			$menu = new FrontMenu();
			$child_id = $menu->get_child_id($id,true);
			if(Db::name('front_menu')->where('id','in',$child_id)->delete()){
	        	$this->success('菜单删除成功',url('admin/Sys/front_menu'));
	        }else{
	        	$this->error('菜单删除失败',url('admin/Sys/front_menu'));
	        }
		}else{
	   	  $this->error('提交方式不正确',url('admin/Sys/front_menu'));
	    }
	}
	/**
	 * 更新排序
	 * @return [type] [description]
	 */
	public function front_menu_sort(){
        if(Request::instance()->isAjax()){
        	$data = input('post.');
        	foreach($data as $k => $v){
        		Db::name('front_menu')->where(['id'=>$k])->setField('sort',$v);	
        	}
        	$this->success('排序更新成功',url('admin/Sys/front_menu'));
        }else{
	   	  $this->error('提交方式不正确',url('admin/Sys/front_menu'));
	    }
	}
	/**
	 * 修改状态
	 * @return [type] [description]
	 */
	public function front_menu_status(){
        $id = input('get.id');
        $role = new AuthRule();
        $res = $role->menu_status($id);
        return $res;
	}	
	/**
	 * 更改菜单位置
	 * @return [type] [description]
	 */
    public function front_menu_place(){
       if (!request()->isAjax()) {
            $this->error('提交方式不正确', url('admin/Sys/front_menu'));
        } else {
        	$id = input('n_id');
        	$place = input('news_cid');
        	if(Db::name('front_menu')->where('id',$id)->setField('place',$place)){
                $this->success('菜单位置修改成功');
        	}else{
               $this->error('菜单位置修改失败');
        	}
        }
    }	
    /**
     * 设置管理员个人信息
     * @return [type] [description]
     */
    public function profile(){
    	$id = session('admin_auth.id');
    	$admin = Db::name('admin')->find($id);
    	$this->assign('admin',$admin);
    	return $this->fetch();
    }  

}