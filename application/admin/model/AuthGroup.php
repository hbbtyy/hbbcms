<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-22 11:49:45
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-01-23 22:07:33
 */
namespace app\admin\model;
use think\Model;
use think\Validate;
use think\Request;
use think\Db;

class AuthGroup extends Model{
	/**
	 * 添加角色
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function role_add($data){
         $rule = [
          'title'  => 'require|unique:auth_group',
	      ];

	      $msg = [
	          'title.require' => '角色名不能为空',
	          'title.unique' => '角色名不能重复',
	      ];
	      
	      $data['addtime'] = time();
        $data['rules'] = join(',',$data['rules']);
	      $validate = new Validate($rule, $msg);
	      $result   = $validate->check($data);
	      if($result){
	         if(self::save($data)){
	            return true;
	         }else{
	            $this->error = "添加角色失败";
	            return false;
	         }
	      }else{
	         $this->error = $validate->getError();
	         return false;
	      }
	}
	/**
   * 更新角色状态
   * @param  [type] $id [管理员id]
   * @return [type]     [description]
   */
  public function role_status($id){
      $status = $this->where('id',$id)->value('status');
      $res = [];
      if($status == '1'){
        if($this->where('id',$id)->setField('status','0')){
          $res = ['code'=>'1','msg'=>'已禁用','btn'=>'0'];
        }else{
          $res = ['code'=>'0','msg'=>'修改失败'];
        }
      }else{
          if($this->where('id',$id)->setField('status','1')){
          $res = ['code'=>'1','msg'=>'已开启','btn'=>'1'];
        }else{
          $res = ['code'=>'0','msg'=>'修改失败'];
        }
      }
      return $res;
  }
   /**
   * 角色修改
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function role_edit($data){
     if($this->where(['id'=>['neq',$data['id']],'title'=>$data['title']])->find()){
         $this->error = "角色名已存在";
         return false;
     }
     if(!isset($data['status'])){
        $data['status'] = '0';
     }
     $data['rules'] = join(',',$data['rules']);
     if(self::update($data)){
         return true;
     }else{
         $this->error = "修改角色失败";
         return false;
      }
  }
}