<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-29 14:02:36
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 16:11:41
 */
namespace app\admin\model;
use think\Model;
use think\Validate;
use think\Request;
use think\Db;

class FrontMenu extends Model{
	/**
	 * 添加菜单
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function menu_add($data){
	      $data['addtime'] = time();
          if($data['pid'] == '0'){
          	 $data['level'] = 1;
          }else{
          	 $level = $this->where('id',$data['pid'])->value('level');
             $data['level'] = $level + 1;
          }
         if(self::save($data)){
            return true;
         }else{
            $this->error = "添加菜单失败";
            return false;
         }
	     
	}	
	public function menu_edit($data){
		  if($data['pid'] == $data['id']){
				$this->error = "菜单上一级不能是自己";
                return false;		  	  
		  }
		  //判断是否更改了pid
		  $menu = $this->where('id',$data['id'])->field('pid,level')->find();
		  if(!isset($data['status'])) $data['status'] = 0;
		  if($menu['pid'] == $data['pid']){//未更改pid
		  	  if(self::update($data)){
		          return true;
		      }else{
		         $this->error = "修改菜单失败";
		         return false;
		      }
		  }else{
		  	$id = $data['id'];
		  	//找到移动到此的level
		  	if($data['pid'] == 0){
		  		$get_level = 1;
		  		$data['level'] = 1;
		  	}else{
		  		$get_level = $this->where('id',$data['pid'])->value('level');
		  		$data['level'] = $get_level + 1;
		  	}
		  	$level = $menu['level'];//当前的level
		  	if(self::update($data)){
		  		$childs_ids = $this-> get_child_id($data['id']);
		  		if($level > $get_level){ //放到比之前level低的，即由3-1
					//修改子类
                	if($data['pid'] == 0){
                		$sub_level_dec = $level - $get_level;
                	}else{
                		$sub_level_dec = $level - $get_level - 1;
                	}
                	$this->where('id','in',$childs_ids)->setDec('level',$sub_level_dec);		  			
		  		}else{    //放到比之前level高的的，即由1-3
					//修改子类
                	$sub_level_dec = $get_level - $level + 1;
                	$this->where('id','in',$childs_ids)->setInc('level',$sub_level_dec);		  			
		  		}
                return true;
			}else{
                $this->error = "修改菜单失败";
                return false;
            }		  		

		  }    
	      
	}
	/**
   * 更新菜单状态
   * @param  [type] $id [管理员id]
   * @return [type]     [description]
   */
	  public function menu_status($id){
	      $status = $this->where('id',$id)->value('status');
	      $res = [];
	      if($status == '1'){
	        if($this->where('id',$id)->setField('status','0')){
	          $res = ['code'=>'1','msg'=>'隐藏','btn'=>'0','url'=>url('admin/Sys/front_menu')];
	        }else{
	          $res = ['code'=>'0','msg'=>'修改失败'];
	        }
	      }else{
	          if($this->where('id',$id)->setField('status','1')){
	          $res = ['code'=>'1','msg'=>'显示','btn'=>'1','url'=>url('admin/Sys/front_menu')];
	        }else{
	          $res = ['code'=>'0','msg'=>'修改失败'];
	        }
	      }
	      return $res;
	  }	
	/**
	 * 得到包含自己以及所有子类的id
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function get_child_id($id,$self = 'false'){
        $menu_all = self::select();
        $tree = new \Tree();
		$tree->init($menu_all);
		$child_id = $tree->get_child_id($id);
		if($self == 'true'){
			array_push($child_id,$id);
		}
		return $child_id;
	}	  
}