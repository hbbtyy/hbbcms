<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-03-01 14:31:57
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-06 11:39:57
 */
namespace app\admin\model;

use think\Model;
use think\Db;

/**
 * 后台插件模型
 * @package app\admin\model
 */
class Hooks extends Model{
	public function add_hooks($addons_name){
         $addons_class = get_addon_class($addons_name);//获取插件名
        if(!class_exists($addons_class)){
            $this->error = "未实现{$addons_name}插件的入口文件";
            return false;
        }
        $methods = get_class_methods($addons_class);
        $hooks = self::column('name');
        $common = array_intersect($hooks, $methods);
        if(!empty($common)){
            foreach ($common as $hook) {
                $flag = self::update_addons($hook, array($addons_name));
                if(false === $flag){
                    self::remove_hooks($addons_name);
                    return false;
                }
            }
        } else {
             $this->error = '插件未实现任何钩子';
            return false;
        }
        return true;
	}
	 /**
     * 更新单个钩子处的插件
     */
    public function update_addons($hook_name, $addons_name){
        $o_addons = self::where('name',$hook_name)->value('addons');
        if($o_addons)
            $o_addons = explode(',',$o_addons);
        if($o_addons){
            $addons = array_merge($o_addons, $addons_name);
            $addons = array_unique($addons);
        }else{
            $addons = $addons_name;
        }
        $flag = Db::name('Hooks')->where('name',$hook_name)->setField('addons',implode(',',$addons));
        if(false === $flag)
            Db::name('Hooks')->where('name',$hook_name)->setField('addons',implode(',',$o_addons));
        return $flag;
    }

    /**
     * 去除插件所有钩子里对应的插件数据
     */
    public function remove_hooks($addons_name){
        $addons_class = get_addon_class($addons_name);
        if(!class_exists($addons_class)){
            return false;
        }
        $methods = get_class_methods($addons_class);
        $hooks = self::column('name');
        $common = array_intersect($hooks, $methods);
        if($common){
            foreach ($common as $hook) {
                $flag = self::remove_addons($hook, array($addons_name));
                if(false === $flag){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 去除单个钩子里对应的插件数据
     */
    public function remove_addons($hook_name, $addons_name){
        $o_addons = self::where('name',$hook_name)->value('addons');
        $o_addons = explode(',',$o_addons);
        if($o_addons){
            $addons = array_diff($o_addons, $addons_name);
        }else{
            return true;
        }
        $flag = Db::name('Hooks')->where('name',$hook_name)->setField('addons',implode(',',$addons));
        if(false === $flag)
            Db::name('Hooks')->where('name',$hook_name)->setField('addons',implode(',',$o_addons));
        return $flag;
    }

}