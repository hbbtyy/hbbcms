<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-02-25 21:08:53
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-02-26 15:34:50
 */
namespace app\admin\model;
use think\Model;
use think\Request;
use think\Db;

class News extends Model{
  /**
   * 得到新闻
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
    public function get_news($data,$trash = '0'){
        $where = [];
        $cid = isset($data['cid'])?$data['cid']:'';
        $type = isset($data['type'])?$data['type']:'';
        $addtime = isset($data['addtime'])?$data['addtime']:'';
        $title = isset($data['title'])?$data['title']:'';
        $status = isset($data['status'])?$data['status']:'';
        if($cid != '') $where['cid'] = $cid;
        $where['trash'] = $trash;
        if($status != '') $where['status'] = $status;
        if($type != '') $where['type'] = ['like','%'.$type.'%'];
        if($title != '') $where['title'] = ['like','%'.$title.'%'];
        if($addtime != ''){
            $time = explode(' - ',$data['addtime']);
            $start_time = strtotime($time[0]);
            $end_time   = strtotime($time[1]);
            $where['addtime'] = ['between',[$start_time,$end_time]];
        }
        $news = self::where($where)->paginate(config('paginate.list_rows'));
        return $news;
    }
    /**
   * 更新新闻状态
   * @param  [type] $id [管理员id]
   * @return [type]     [description]
   */
      public function news_status($id){
          $status = $this->where('id',$id)->value('status');
          $res = [];
          if($status == '1'){
            if($this->where('id',$id)->setField('status','0')){
              $res = ['code'=>'1','msg'=>'隐藏','btn'=>'0','url'=>url('admin/News/index')];
            }else{
              $res = ['code'=>'0','msg'=>'修改失败'];
            }
          }else{
              if($this->where('id',$id)->setField('status','1')){
              $res = ['code'=>'1','msg'=>'显示','btn'=>'1','url'=>url('admin/News/index')];
            }else{
              $res = ['code'=>'0','msg'=>'修改失败'];
            }
          }
          return $res;
      }    
    /**
     * 添加新闻
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function news_add($data){
       $data['type'] = join(',',$data['type']);
       $data['addtime'] = strtotime($data['addtime']);
       if(request()->file('img')){
       	$img = upload_img(request()->file('img'),'news');
          $data['img'] = $img['filename'];
       }
       $data['userpost'] = Db::name('admin')->where('id',session('admin_user.id'))->value('username');
       if(self::save($data)){
               Db::name('news_cate')->where('id',$data['cid'])->setInc('num');
               $source = Db::name('news_source')->where('name',$data['source'])->find();
               if($source){
               	Db::name('news_source')->where('id',$source['id'])->setInc('times');
               }else{
               	$source_data = [
                           'name' => $data['source'],
                           'times' => '1'
               	];
               	Db::name('news_source')->data($source_data)->insert();
               }
              return true;
           }else{
              $this->error = "添加新闻失败";
              return false;
           }
    } 
    /**
     * 修改新闻
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function news_edit($data){
       $data['type'] = join(',',$data['type']);
       $data['addtime'] = strtotime($data['addtime']);
       if(request()->file('img')){
          $img = upload_img(request()->file('img'),'news');
          $data['img'] = $img['filename'];
       }else{
          unset($data['img']);
       }
       //得到原来的新闻
       $news = self::get($data['id']);
       $data['userpost'] = $news['userpost'];
       if(self::where('id',$data['id'])->update($data)){
               if($news['cid'] != $data['cid']){
                  Db::name('news_cate')->where('id',$data['cid'])->setInc('num');
                  Db::name('news_cate')->where('id',$news['cid'])->setDec('num');
               }
               if($news['source'] != $data['source']){
                    $source = Db::name('news_source')->where('name',$data['source'])->find();
                   if($source){
                     Db::name('news_source')->where('id',$source['id'])->setInc('times');
                   }else{
                     $source_data = [
                               'name' => $data['source'],
                               'times' => '1'
                     ];
                     Db::name('news_source')->data($source_data)->insert();
                   }
               }
              
              return true;
           }else{
              $this->error = "修改新闻失败";
              return false;
           }
    }   
}