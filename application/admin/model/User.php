<?php

/**
 * @Author: CraspH2b
 * @Date:   2018-01-20 14:49:53
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-03-29 11:06:28
 */
namespace app\admin\model;
use think\Model;
use think\Validate;
use think\Request;
use think\Db;

class User extends Model{
  /**
   * 添加用户
   * @return [type] [description]
   */
  public function user_add($data){
      $rule = [
          'username'  => 'require|alphaNum|token|unique:user',
          'nickname'  => 'require|unique:user',
          'tel'   => 'number|length:11',
          'email' => 'email',
      ];

      $msg = [
          'username.require' => '用户名不能为空',
          'username.token' => '表单不能重复提交',
          'username.alphaNum'     => '用户只能是字母或者数字组成',
          'username.unique'   => '用户已存在',
		  'nickname.require' => '昵称不能为空',
		  'nickname.unique'   => '昵称已存在',          
          'tel.number'  => '电话必须是数字',
          'tel.length'  => '电话只能是11位',
          'email'        => '邮箱格式不正确',
      ];
     //生成密码
      $data['pwd_mix'] = get_mix();
      if(!isset($data['password'])) $data['password'] = '123465';
      $data['password'] = get_password($data['pwd_mix'],$data['password']);
      $validate = new Validate($rule, $msg);
      $result   = $validate->check($data);
      if($result){
         $data['pwd_mix'] = get_mix();
         $data['password'] = get_password($data['pwd_mix'],$data['password']);
         $data['addtime'] = time();
         unset($data['__token__']);
         if(self::save($data)){
            return true;
         }else{
            $this->error = "添加用户失败";
            return false;
         }
      }else{
         $this->error = $validate->getError();
         $this->data = Request::instance()->token();
         return false;
      }
  }   
  /**
   * 用户修改
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function user_edit($data){
     if($this->where(['id'=>['neq',$data['id']],'username'=>$data['username']])->find()){
         $this->error = "用户名已存在";
         return false;
     }
     if(!isset($data['password'])){
        unset($data['password']);
     }
     if($data['password']){
         $data['pwd_mix'] = get_mix();
         $data['password'] = get_password($data['pwd_mix'],$data['password']);
     }
     if(!isset($data['status'])){
        $data['status'] = '0';
     }
     if(self::update($data)){
         return true;
     }else{
         $this->error = "修改用户失败";
         return false;
      }
  }
  /**
   * 更新用户状态
   * @param  [type] $id [管理员id]
   * @return [type]     [description]
   */
  public function user_status($id){
      $status = $this->where('id',$id)->value('status');
      $res = [];
      if($status == '1'){
        if($this->where('id',$id)->setField('status','0')){
          $res = ['code'=>'1','msg'=>'已禁用','btn'=>'0'];
        }else{
          $res = ['code'=>'0','msg'=>'修改失败'];
        }
      }else{
          if($this->where('id',$id)->setField('status','1')){
          $res = ['code'=>'1','msg'=>'已开启','btn'=>'1'];
        }else{
          $res = ['code'=>'0','msg'=>'修改失败'];
        }
      }
      return $res;
  }  
}