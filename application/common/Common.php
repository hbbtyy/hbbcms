<?php

/**
 * @Author: CraspHB彬
 * @Date:   2018-06-28 14:48:24
 * @Email:   646054215@qq.com
 * @Last Modified time: 2018-06-28 14:56:41
 */
namespace app\common;
use think\Controller;

class Common extends Controller{

	public function _initialize(){
		//判断是否安装
		if(!file_exists("./data/install.lock")){
			$this->redirect(url('Install/index/index'));
		}

	}
}