<?php 
 return array (
  'web_log' => 
  array (
    'status' => '0',
    'web_log_request' => 
    array (
      0 => 'GET',
      1 => 'POST',
      2 => 'AJAX',
    ),
    'web_log_on' => '0',
    'web_log_controller' => 
    array (
    ),
  ),
  'web_basic' => 
  array (
    'web_on' => '1',
    'web_copyright' => 'Copyright © ******有限公司 All rights reserved.',
    'site_statistics' => '<script>alert(1)</script>',
  ),
  'prefix' => 'hbb_',
  'datacon' => 
  array (
    'filePath' => './data/database',
    'partSize' => '2097152',
    'gzip' => '1',
    'compress' => '9',
  ),
  'web_upload' => 
  array (
    'upload_path' => './public/upload',
    'file_max_size' => '2097152',
    'file_allow_suf' => 'doc,docx,txt,pdf,xlex,xle',
    'img_max_size' => '2097152',
    'img_allow_suf' => 'jpg,png,jpeg,bmp,gif',
    'thumb_min_width' => '150',
    'thumb_min_height' => '150',
    'thumb_mid_width' => '350',
    'thumb_mid_height' => '350',
    'thumb_max_width' => '450',
    'thumb_max_height' => '450',
    'water_type' => '1',
    'water_place' => '9',
    'water_img' => './upload/logo/20180328/6059ca2c423d9a4aae58797d40e3b579.png',
    'water_alpha' => '50',
  ),
  'web_sys' => 
  array (
    'site_name' => 'HBBCMS内容管理框架',
    'site_url' => '',
    'site_logo' => './public/upload/logo/20180628/83b85f22d7bc47fd088506faa15b9766.png',
    'list_row' => '20',
  ),
  'captcha' => 
  array (
    'fontSize' => 14,
    'fontttf' => '4.ttf',
    'length' => 4,
    'useNoise' => '0',
    'useCurve' => '1',
    'imageH' => '36',
    'imageW' => '100',
  ),
  'app_debug' => true,
  'app_trace' => false,
);