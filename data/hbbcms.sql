/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : hbbcms

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-06-27 09:49:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hbb_addons
-- ----------------------------
DROP TABLE IF EXISTS `hbb_addons`;
CREATE TABLE `hbb_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text NOT NULL COMMENT '插件描述',
  `img` varchar(255) NOT NULL COMMENT '缩略图',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of hbb_addons
-- ----------------------------
INSERT INTO `hbb_addons` VALUES ('66', 'Info', '系统信息展示', '展示系统的信息', './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png', '1', '[]', 'CraspHB彬', '1.0', '1520328954', '0');
INSERT INTO `hbb_addons` VALUES ('90', 'Hbbcms', 'HBBCMS信息展示', '展示HBBCMS的信息', './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png', '1', '{\"random\":\"1\",\"editor_wysiwyg\":\"\\u6478\\u6478\\u5927\\uff0c\\u54c8\\u54c8\\u54c8\\u54c8\",\"editor_bbb\":\"\\u6478\\u6478\\u5927\\uff0c\\u54c8\\u54c8\\u54c8\\u54c8\"}', 'CraspHB彬', '1.0', '1522373013', '0');
INSERT INTO `hbb_addons` VALUES ('89', 'develop', '调试模式设置', '设置调试模式', './upload/logo/20180206/783e494b62f567ba0b99fe7b777ecd57.png', '1', 'null', 'CraspHB彬', '1.0', '1522293823', '0');

-- ----------------------------
-- Table structure for hbb_admin
-- ----------------------------
DROP TABLE IF EXISTS `hbb_admin`;
CREATE TABLE `hbb_admin` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL COMMENT '昵称',
  `truename` varchar(15) NOT NULL COMMENT '真实姓名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `pwd_mix` varchar(10) NOT NULL COMMENT '加密混淆',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0：男，1：女',
  `avatar` varchar(100) NOT NULL COMMENT '头像',
  `tel` varchar(12) NOT NULL COMMENT '电话',
  `email` varchar(20) NOT NULL COMMENT '邮箱',
  `times` smallint(5) unsigned NOT NULL COMMENT '登录次数',
  `login_ip` varchar(15) NOT NULL COMMENT '登录ip',
  `last_login_ip` varchar(15) NOT NULL COMMENT '上次登录ip',
  `login_time` varchar(15) NOT NULL COMMENT '登录时间',
  `last_login_time` varchar(15) NOT NULL COMMENT '上次登录时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，0：锁定，1：开放',
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='管理员表';


-- ----------------------------
-- Table structure for hbb_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hbb_auth_group`;
CREATE TABLE `hbb_auth_group` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色表',
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `rules` char(80) NOT NULL DEFAULT '',
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of hbb_auth_group
-- ----------------------------
INSERT INTO `hbb_auth_group` VALUES ('1', '超级管理员', '1', '1,19,20,21,22,23,26,27,30,31,34,33,32,35,36,37,38,39,40,41,42,2,3,6,7,8,10,11,12', '1516599819');

-- ----------------------------
-- Table structure for hbb_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `hbb_auth_group_access`;
CREATE TABLE `hbb_auth_group_access` (
  `uid` tinyint(1) unsigned NOT NULL COMMENT '权限表',
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of hbb_auth_group_access
-- ----------------------------
INSERT INTO `hbb_auth_group_access` VALUES ('1', '1');

-- ----------------------------
-- Table structure for hbb_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hbb_auth_rule`;
CREATE TABLE `hbb_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `title` varchar(20) NOT NULL,
  `css` varchar(15) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `notcheck` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1：不检测，0：检测',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` varchar(100) NOT NULL,
  `pid` mediumint(8) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  `sort` tinyint(1) unsigned NOT NULL,
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`,`sort`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='菜单列表';

-- ----------------------------
-- Records of hbb_auth_rule
-- ----------------------------
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (1, 'admin/Index/index', '控制台', 'fa-tachometer', 1, 1, 1, '', 0, 1, 10, 1516628708);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (2, 'admin/Sys', '系统管理', 'fa-desktop', 1, 1, 1, '', 0, 1, 80, 1516628752);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (3, 'admin/Sys/admin', '管理员列表', '', 1, 1, 1, '', 2, 2, 10, 1516632245);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (4, 'admin/Sys/admin_role', '角色列表', '', 1, 1, 1, '', 2, 2, 40, 1516632271);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (5, 'admin/Sys/menu', '菜单列表', '', 1, 1, 1, '', 2, 2, 30, 1516632300);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (6, 'admin/Sys/admin_add', '添加管理员', '', 1, 1, 0, '', 3, 3, 10, 1516632780);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (7, 'admin/Sys/admin_runadd', '添加管理员操作', '', 1, 1, 0, '', 3, 3, 20, 1516632830);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (8, 'admin/Sys/admin_edit', '修改管理员', '', 1, 1, 0, '', 3, 3, 30, 1516707376);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (10, 'admin/Sys/admin_runedit', '修改管理员操作', '', 1, 1, 0, '', 3, 3, 40, 1516710060);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (11, 'admin/Sys/admin_del', '删除管理员', '', 1, 1, 0, '', 3, 3, 50, 1516712972);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (12, 'admin/Sys/admin_status', '修改管理员状态', '', 1, 1, 0, '', 3, 3, 60, 1516713009);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (13, 'admin/Sys/menu_add', '添加菜单', '', 1, 1, 0, '', 5, 3, 10, 1516760504);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (14, 'admin/Sys/menu_runedit', '修改菜单操作', '', 1, 1, 0, '', 5, 3, 10, 1516760602);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (15, 'admin/Sys/menu_runadd', '添加菜单操作', '', 1, 1, 0, '', 5, 3, 10, 1516760681);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (19, 'admin/Database', '数据库管理', 'fa-database', 1, 1, 1, '', 0, 1, 10, 1516761685);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (16, 'admin/Sys/menu_edit', '修改菜单', '', 1, 1, 0, '', 5, 3, 10, 1516760713);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (17, 'admin/Sys/menu_del', '删除菜单', '', 1, 1, 0, '', 5, 3, 10, 1516760738);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (18, 'admin/Sys/menu_status', '修改菜单状态', '', 1, 1, 0, '', 5, 3, 10, 1516760764);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (20, 'admin/Database/index', '数据库列表', '', 1, 1, 1, '', 19, 2, 10, 1516761953);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (21, 'admin/Database/optimize', '数据库优化', '', 1, 1, 0, '', 20, 3, 10, 1516801898);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (22, 'admin/Sys/Database/repair', '数据库修复', '', 1, 1, 0, '', 20, 3, 10, 1516801931);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (23, 'admin/Database/show_back', '数据库备份表', '', 1, 1, 1, '', 19, 2, 40, 1516956288);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (27, 'admin/Sys/web_log', '日志列表', '', 1, 1, 1, '', 26, 2, 10, 1517032543);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (26, 'admin/Sys', '日志管理', 'fa-book', 1, 1, 1, '', 0, 1, 30, 1517032521);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (30, 'admin/News', '新闻管理', 'fa-bookmark', 1, 1, 1, '', 0, 1, 20, 1519476836);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (29, 'admin/Sys/sys_basic_set', '系统设置', '', 1, 1, 1, '', 2, 2, 10, 1517830150);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (32, 'admin/News/news_cate', '新闻分类列表', '', 1, 1, 1, '', 30, 2, 40, 1519478191);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (31, 'admin/News/index', '新闻列表', '', 1, 1, 1, '', 30, 2, 10, 1519476869);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (33, 'admin/News/news_add', '添加新闻', '', 1, 1, 1, '', 30, 2, 20, 1519615239);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (34, 'admin/News/news_trash', '新闻草稿箱', '', 1, 1, 1, '', 30, 2, 10, 1519630128);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (35, 'admin/Addons', '插件管理', 'fa-cogs ', 1, 1, 1, '', 0, 1, 50, 1519883611);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (36, 'admin/Addons/hooks', '钩子列表', '', 1, 1, 1, '', 35, 2, 10, 1519883633);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (37, 'admin/Addons/index', '插件列表', '', 1, 1, 1, '', 35, 2, 10, 1519895587);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (38, 'admin/addons/config', '插件设置', '', 1, 1, 0, '', 37, 3, 10, 1522221400);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (39, 'admin/User', '用户管理', 'fa-user ', 1, 1, 1, '', 0, 1, 60, 1522224952);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (40, 'admin/User/index', '用户列表', '', 1, 1, 1, '', 39, 2, 10, 1522224975);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (41, 'admin/Sys', '前台管理', 'fa-leaf ', 1, 1, 1, '', 0, 1, 70, 1522301885);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (42, 'admin/Sys/front_menu', '前台菜单列表', '', 1, 1, 1, '', 41, 2, 10, 1522301908);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (43, 'admin/Addons/install', '安装插件', '', 1, 1, 0, '', 37, 3, 10, 1530513557);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (44, 'admin/Addons/uninstall', '卸载插件', '', 1, 1, 0, '', 37, 3, 10, 1530513594);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (45, 'admin/Addons/set_config', '保存插件设置', '', 1, 1, 0, '', 37, 3, 10, 1530513625);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (46, 'admin/Addons/addons_add', '添加钩子', '', 1, 1, 0, '', 36, 3, 10, 1530513668);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (47, 'admin/Addons/addons_runadd', '添加钩子操作', '', 1, 1, 0, '', 36, 3, 10, 1530513733);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (48, 'admin/Addons/addons_edit', '修改钩子', '', 1, 1, 0, '', 36, 3, 10, 1530513812);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (49, 'admin/Addons/addons_runedit', '修改钩子操作', '', 1, 1, 0, '', 36, 3, 10, 1530513831);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (50, 'admin/Addons/addons_del', '删除钩子', '', 1, 1, 0, '', 36, 3, 10, 1530513850);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (51, 'admin/Addons/status', '修改插件状态', '', 1, 1, 0, '', 37, 3, 10, 1530513966);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (52, 'admin/Database/del', '删除备份文件', '', 1, 1, 0, '', 23, 3, 10, 1530514044);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (53, 'admin/Database/export', '备份数据库', '', 1, 1, 1, '', 20, 3, 10, 1530514074);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (54, 'admin/Database/import', '还原数据库', '', 1, 1, 0, '', 23, 3, 10, 1530514098);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (55, 'admin/News/news_trash_add', '移入回收站', '', 1, 1, 0, '', 31, 3, 10, 1530514180);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (56, 'admin/News/news_trash_remove', '回收站新闻发布', '', 1, 1, 0, '', 34, 3, 10, 1530514224);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (57, 'admin/News/news_trash_edit', '修改回收站新闻', '', 1, 1, 0, '', 34, 3, 10, 1530514237);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (58, 'admin/News/news_trash_runedit', '修改回收站新闻操作', '', 1, 1, 0, '', 34, 3, 10, 1530514247);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (59, 'admin/News/news_runadd', '添加新闻操作', '', 1, 1, 0, '', 31, 3, 10, 1530514320);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (60, 'admin/News/news_edit', '修改新闻', '', 1, 1, 0, '', 31, 3, 10, 1530514338);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (61, 'admin/News/news_runedit', '修改新闻操作', '', 1, 1, 0, '', 31, 3, 10, 1530514346);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (62, 'admin/News/news_del', '删除新闻', '', 1, 1, 0, '', 31, 3, 10, 1530514355);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (63, 'admin/News/news_sort', '更新新闻排序', '', 1, 1, 0, '', 31, 3, 10, 1530514369);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (64, 'admin/News/news_cid', '更改新闻分类', '', 1, 1, 0, '', 31, 3, 10, 1530514378);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (65, 'admin/News/news_status', '更改新闻状态', '', 1, 1, 0, '', 31, 3, 10, 1530514391);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (66, 'admin/News/news_cate_add', '添加新闻分类', '', 1, 1, 0, '', 32, 3, 10, 1530514443);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (67, 'admin/News/news_cate_runadd', '添加新闻分类操作', '', 1, 1, 0, '', 32, 3, 10, 1530514449);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (68, 'admin/News/news_cate_edit', '修改新闻分类', '', 1, 1, 0, '', 32, 3, 10, 1530514458);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (69, 'admin/News/news_cate_runedit', '修改新闻分类操作', '', 1, 1, 0, '', 32, 3, 10, 1530514465);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (70, 'admin/News/news_cate_del', '删除新闻分类', '', 1, 1, 0, '', 32, 3, 10, 1530514480);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (71, 'admin/News/news_cate_sort', '新闻分类排序', '', 1, 1, 0, '', 32, 3, 10, 1530514491);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (72, 'admin/News/news_cate_status', '更改新闻分类状态', '', 1, 1, 0, '', 32, 3, 10, 1530514505);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (73, 'admin/Sys/admin_role_add', '添加角色', '', 1, 1, 0, '', 4, 3, 10, 1530514575);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (74, 'admin/Sys/admin_role_runadd', '添加角色操作', '', 1, 1, 0, '', 4, 3, 10, 1530514585);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (75, 'admin/Sys/admin_role_edit', '修改角色', '', 1, 1, 0, '', 4, 3, 10, 1530514593);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (76, 'admin/Sys/admin_role_runedit', '修改角色操作', '', 1, 1, 0, '', 4, 3, 10, 1530514601);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (77, 'admin/Sys/admin_role_status', '修改角色状态', '', 1, 1, 0, '', 4, 3, 10, 1530514612);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (78, 'admin/Sys/admin_role_del', '删除角色', '', 1, 1, 0, '', 4, 3, 10, 1530514621);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (79, 'admin/Sys/menu_sort', '菜单排序', '', 1, 1, 0, '', 5, 3, 10, 1530514694);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (80, 'admin/Sys/web_log_runset', '日志设置操作', '', 1, 1, 0, '', 27, 3, 10, 1530515268);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (81, 'admin/Sys/web_log_del', '日志设置删除', '', 1, 1, 0, '', 27, 3, 10, 1530515290);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (82, 'admin/Sys/sys_basic_runset', '系统基本参数设置操作', '', 1, 1, 0, '', 29, 3, 10, 1530515393);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (83, 'admin/Sys/sys_basic_runsys', '系统设置操作', '', 1, 1, 0, '', 29, 3, 10, 1530516859);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (84, 'admin/Sys/sys_basic_database', '数据库设置', '', 1, 1, 0, '', 29, 3, 10, 1530516878);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (85, 'admin/Sys/sys_basic_rundatabase', '数据库设置操作', '', 1, 1, 0, '', 29, 3, 10, 1530516888);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (86, 'admin/Sys/sys_upload_set', '上传设置', '', 1, 1, 0, '', 29, 3, 10, 1530516898);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (87, 'admin/Sys/sys_upload_runset', '上传设置操作', '', 1, 1, 0, '', 29, 3, 10, 1530516906);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (88, 'admin/Sys/front_menu_add', '添加前台菜单', '', 1, 1, 0, '', 41, 2, 10, 1530517003);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (89, 'admin/Sys/front_menu_runadd', '添加前台菜单操作', '', 1, 1, 0, '', 41, 2, 10, 1530517012);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (90, 'admin/Sys/front_menu_edit', '修改前台菜单', '', 1, 1, 0, '', 41, 2, 10, 1530517022);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (91, 'admin/Sys/front_menu_runedit', '修改前台菜单操作', '', 1, 1, 0, '', 41, 2, 10, 1530517029);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (92, 'admin/Sys/front_menu_del', '删除前台菜单', '', 1, 1, 0, '', 41, 2, 10, 1530517044);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (93, 'admin/Sys/front_menu_sort', '前台菜单排序', '', 1, 1, 0, '', 41, 2, 10, 1530517055);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (94, 'admin/Sys/front_menu_status', '修改前台菜单状态', '', 1, 1, 0, '', 41, 2, 10, 1530517066);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (95, 'admin/Sys/front_menu_place', '修改前台菜单位置', '', 1, 1, 0, '', 41, 2, 10, 1530517079);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (96, 'admin/User/user_add', '添加用户', '', 1, 1, 0, '', 39, 2, 10, 1530517134);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (97, 'admin/User/user_runadd', '添加用户操作', '', 1, 1, 0, '', 39, 2, 10, 1530517140);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (98, 'admin/User/user_edit', '修改用户', '', 1, 1, 0, '', 39, 2, 10, 1530517151);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (99, 'admin/User/user_runedit', '修改用户操作', '', 1, 1, 0, '', 39, 2, 10, 1530517157);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (100, 'admin/User/user_del', '删除用户', '', 1, 1, 0, '', 39, 2, 10, 1530517167);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (101, 'admin/User/user_status', '修改用户状态', '', 1, 1, 0, '', 39, 2, 10, 1530517176);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (102, 'admin/Umeditor', '编辑器管理', '', 1, 1, 0, '', 0, 1, 100, 1530599364);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (103, 'admin/Umeditor/upload', '上传', '', 1, 1, 0, '', 102, 2, 10, 1530599402);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (104, 'admin/Umeditor/_upload_editor', '文件上传', '', 1, 1, 0, '', 102, 2, 10, 1530599417);
INSERT INTO `hbb_auth_rule` (`id`, `name`, `title`, `css`, `type`, `notcheck`, `status`, `condition`, `pid`, `level`, `sort`, `addtime`) VALUES (105, 'admin/Umeditor/uploadscrawl', '涂鸦', '', 1, 1, 0, '', 102, 2, 10, 1530599428);


-- ----------------------------
-- Table structure for hbb_database
-- ----------------------------
DROP TABLE IF EXISTS `hbb_database`;
CREATE TABLE `hbb_database` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(40) NOT NULL,
  `filename` varchar(800) NOT NULL,
  `filesize` varchar(10) NOT NULL,
  `type` varchar(10) NOT NULL,
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='数据库表';



-- ----------------------------
-- Table structure for hbb_front_menu
-- ----------------------------
DROP TABLE IF EXISTS `hbb_front_menu`;
CREATE TABLE `hbb_front_menu` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(10) NOT NULL COMMENT '菜单名称',
  `css` varchar(15) NOT NULL COMMENT '图标',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0：模块，1：外部链接',
  `links` varchar(100) NOT NULL COMMENT '链接',
  `place` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0：顶部，1：尾部',
  `pid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '级别',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `target` varchar(10) NOT NULL COMMENT '打开方式，新窗口or元窗口',
  `sort` tinyint(1) unsigned NOT NULL DEFAULT '50' COMMENT '排序',
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `place` (`place`),
  KEY `place_2` (`place`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='前台菜单表';


-- ----------------------------
-- Table structure for hbb_hooks
-- ----------------------------
DROP TABLE IF EXISTS `hbb_hooks`;
CREATE TABLE `hbb_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hbb_hooks
-- ----------------------------
INSERT INTO `hbb_hooks` VALUES ('2', 'adminIndex', '后台首页显示', '1', '1520318431', 'info,hbbcms');
INSERT INTO `hbb_hooks` VALUES ('3', 'adminIndexRight', '首页左边的插件', '1', '1520819243', 'develop');

-- ----------------------------
-- Table structure for hbb_news
-- ----------------------------
DROP TABLE IF EXISTS `hbb_news`;
CREATE TABLE `hbb_news` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `sort` tinyint(1) unsigned NOT NULL COMMENT '排序',
  `cid` tinyint(1) unsigned NOT NULL COMMENT '新闻栏目id',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `keywords` varchar(100) NOT NULL COMMENT '关键字，多个用，号隔开',
  `description` varchar(300) NOT NULL COMMENT '描述',
  `content` text NOT NULL COMMENT '内容',
  `addtime` int(11) unsigned NOT NULL COMMENT '添加时间',
  `type` varchar(15) NOT NULL COMMENT '种类：r热门、t推荐、z置顶、y原创',
  `source` varchar(15) NOT NULL COMMENT '来源',
  `readc` mediumint(5) unsigned NOT NULL COMMENT '阅读量',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `userpost` varchar(15) NOT NULL COMMENT '发送人',
  `img` varchar(255) DEFAULT NULL COMMENT '文章缩略图',
  `trash` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否在回收站',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`,`type`,`status`),
  KEY `sort` (`sort`),
  KEY `trash` (`trash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of hbb_news
-- ----------------------------

INSERT INTO `hbb_news` (`id`, `sort`, `cid`, `title`, `keywords`, `description`, `content`, `addtime`, `type`, `source`, `readc`, `status`, `userpost`, `img`, `trash`) VALUES
(2, 10, 2, '输急眼！厄齐尔同球迷激烈对喷 网友：他是骗子', '世界杯,德国', '输急眼！厄齐尔同球迷激烈对喷 网友：他是骗子', '<p><img src=\"/public/upload/umeditor/img/20180628/e5683e6fbba36de89f0cfe9ad88664dc.jpeg\" title=\"e5683e6fbba36de89f0cfe9ad88664dc.jpeg\" alt=\"e5683e6fbba36de89f0cfe9ad88664dc.jpeg\"/></p><p>（搜狐体育讯）北京时间6月27日晚22时，2018年俄罗斯世界杯小组赛E组收官战的一场焦点对决在喀山体育场进行，卫冕冠军德国0-2爆冷不敌亚洲劲旅韩国。据英国媒体《太阳报》报道，德国中场厄齐尔赛后与球迷发生了冲突。</p><p>赛后，一名情绪激动的德国球迷在厄齐尔面前大喊大叫，指责他的表现。厄齐尔也未示弱甚至想上前讨要说法，被他的队友拦住。</p><p>报道称，虽然厄齐尔今天的表现并不算糟糕，他本场比赛创造出了7次机会，是本届世界杯单场创造机会最多的球员。但德国球迷显然认为这还不够，因为他们的球队最终未能小组出线。</p><p>在社交媒体上，许多球迷都对厄齐尔提出了质疑。</p><p>有人写道：”厄齐尔或许是世界上最懒惰的球员。“还有球迷更加直接，“我发这条消息就是为了说明厄齐尔就是个骗子。“</p><p><br/></p>', 1530174300, 'r,t,z', '搜狐', 50, 1, '15515704092', './public/upload/news/20180628/23c3b1292000fc225ab2d41039073369.jpeg', 0);


-- ----------------------------
-- Table structure for hbb_news_cate
-- ----------------------------
DROP TABLE IF EXISTS `hbb_news_cate`;
CREATE TABLE `hbb_news_cate` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL COMMENT '分类名',
  `num` mediumint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文章数',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `sort` tinyint(1) unsigned NOT NULL DEFAULT '20' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`sort`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文章分类表';

-- ----------------------------
-- Records of hbb_news_cate
-- ----------------------------

INSERT INTO `hbb_news_cate` (`id`, `name`, `num`, `status`, `sort`) VALUES
(2, '世界杯', 1, 1, 10);

-- ----------------------------
-- Table structure for hbb_news_source
-- ----------------------------
DROP TABLE IF EXISTS `hbb_news_source`;
CREATE TABLE `hbb_news_source` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL COMMENT '来源名称',
  `times` smallint(5) unsigned NOT NULL COMMENT '使用次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='新闻来源表';


-- ----------------------------
-- Table structure for hbb_updater
-- ----------------------------
DROP TABLE IF EXISTS `hbb_updater`;
CREATE TABLE `hbb_updater` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `clid` smallint(5) unsigned NOT NULL COMMENT '课程id',
  `uname` varchar(50) NOT NULL COMMENT '更新名称',
  `usql` varchar(100) NOT NULL COMMENT 'sql文件位置',
  `addtime` int(11) unsigned NOT NULL COMMENT '添加时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '更新状态',
  PRIMARY KEY (`id`),
  KEY `clid` (`clid`,`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='更新表';


-- ----------------------------
-- Table structure for hbb_user
-- ----------------------------
DROP TABLE IF EXISTS `hbb_user`;
CREATE TABLE `hbb_user` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL COMMENT '用户名',
  `nickname` varchar(15) NOT NULL COMMENT '昵称',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `pwd_mix` varchar(10) NOT NULL COMMENT '密码加密',
  `pic` varchar(100) NOT NULL COMMENT '头像',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0：男，1：女，2：保密',
  `tel` varchar(15) NOT NULL COMMENT '电话',
  `email` varchar(25) NOT NULL COMMENT '邮箱',
  `info` varchar(100) NOT NULL COMMENT '个人简介',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1:开启   0：禁止',
  `last_login_ip` varchar(20) NOT NULL COMMENT '上次登录ip',
  `last_login_time` int(11) unsigned NOT NULL COMMENT '上次登录时间',
  `login_time` int(11) unsigned NOT NULL COMMENT '本次登录时间',
  `times` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `addtime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户表';


-- ----------------------------
-- Table structure for hbb_web_log
-- ----------------------------
DROP TABLE IF EXISTS `hbb_web_log`;
CREATE TABLE `hbb_web_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` tinyint(1) unsigned NOT NULL COMMENT '操作人id',
  `admin_username` varchar(20) NOT NULL COMMENT '操作人',
  `action_name` varchar(40) NOT NULL COMMENT '操作名称',
  `action_ip` varchar(20) NOT NULL COMMENT '操作ip',
  `action_url` varchar(200) NOT NULL COMMENT '操作的url',
  `addtime` int(11) unsigned NOT NULL COMMENT '操作时间',
  `action_param` varchar(400) NOT NULL COMMENT '操作参数',
  `action_method` varchar(10) NOT NULL COMMENT '操作方式',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='日志表';
