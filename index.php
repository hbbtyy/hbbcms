<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]


// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 定义应用目录
define('APP_PATH', __DIR__ . '/application/');
define("RUNTIME_PATH", __DIR__ .'/data/runtime/');
//定义根目录
define("HBBCMS_ROOT",dirname($_SERVER['SCRIPT_NAME']));
define("HBBCMS_DIR",str_replace('\\','/',__DIR__));
// 加载框架引导文件
require __DIR__ . '/thinkphp/start.php';
